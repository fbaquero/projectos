const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

describe('First test',

function(){
    it('Test that DuckDuckgo works', function(done){

      //Prueba 1. Probamos que conecta con la página
      chai.request('http://www.duckduckgo.com')

      //Prueba 2. Probamos el fallo página no existente
      //chai.request('https://developer.mozilla.org/en-US/adsfasdfas')

      .get('/')
      .end(
          function(err,res){
            //console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);


            done();
          }
      )
    }
  )
});

describe('Test de API Usuarios',

function(){
    it('Prueba que la API de usuarios responde correctamente', function(done){

      //Prueba 1. Probamos que conecta con la página
      chai.request('http://localhost:3000')

      //Prueba 2. Probamos el fallo página no existente
      //chai.request('https://developer.mozilla.org/en-US/adsfasdfas')

      .get('/apitechu/v1/hello')
      .end(
          function(err,res){
            //console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU");


            done();
          }
      )
    }
  ),
  it('Prueba que la API de usuarios devuelve una lista de usuarios correctos', function(done){
       chai.request('http://localhost:3000')
         .get('/apitechu/v1/users')
         .end(
           function(err, res) {
             // console.log(res);
             console.log("Request has finished");

             res.should.have.status(200);
             res.body.users.should.be(array);

             for (user of res.body.users) {
               user.should.have.property('email');
               user.should.have.property('first_name');
             }

             // res.body.msg.should.be.eql("Hola desde API TechU");
             done();
           }
         )
     }
   )
});
