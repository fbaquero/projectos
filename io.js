function writeUserDataToFile(data){
  console.log("writeUserDataToFile");

  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",

    function(err){
      if (err){
          console.log(err);
      } else{
        console.log ("Usuario persistido");
      }
    }

  )
}

module.exports.writeUserDataToFile = writeUserDataToFile
