# Imagen raiz Decimos de donde partimos
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copia de archivos de carpeta local a apitechu. Copia todo lo de la carpeta actual al proyecto de apitechu
ADD . /apitechu

#Instalación de dependencias. npm install para la instalación de todas las dependencias necesarias para mi proyecto. El comando RUN se ejecuta dentro de la imagen
RUN npm install

# Abrimos el puerto 3000. Es el puerto que se expone.
EXPOSE 3000

#El CMD se ejecutara cuando lanzas el contenedor con RUN.
CMD ["npm","start"]
