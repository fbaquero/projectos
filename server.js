//Variables y arranque Express()
const express = require('express');
const app = express ();
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


const io =require('./io.js');
const userController = require('./controllers/UserController');
const AuthController = require('./controllers/AuthController');

app.use(express.json());
const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto BIP: " + port);

app.get('/apitechu/v1/hello',
 function(req, res){
   console.log("GET /apitechu/v1/hello");
   res.send({"msg" : "Hola desde API TechU"});

 }
)


//APP.POST ------ :p1:p2 son los parametros
app.post('/apitechu/v1/monstruo/:p1:p2',
  function(req,res){

    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);
module.exports.getUsersV1 = getUsersV1;
    console.log("Body");
    console.log(req.body);

  }
)

//APP.GET ------
app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id',userController.getUsersByIdV2);
//Aqui había una función que nos hemos llevadoa  ./Controllers/UserController.js

//APP.post ------
app.post ('/apitechu/v1/users',userController.createUsersV1);
app.post ('/apitechu/v2/users',userController.createUsersV2);
//Aqui había una función que nos hemos llevadoa  ./Controllers/UserController.js
app.delete('/apitechu/v1/users/:id',userController.deleteUsersV1);
//Aqui había una función que nos hemos llevadoa  ./Controllers/UserController.js
app.post('/apitechu/v1/login',AuthController.loginV1);
app.post('/apitechu/v1/logout/:id',AuthController.logoutV1);



/*Ejercicio de los for.......

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + loginV1users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
  //     console.log("La posicion " + i + " coincide");
  //     users.splice(i, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }createUsersV2
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  // console.log("Usando Array ForEach");
  // users.forEach(function (user, index) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //   }
  // });

  // console.log("Usando Array findIndex");
  // var indexOfElement = users.findIndex(
  //   function(element){
  //     console.log("comparando " + element.id + " y " +   req.params.id);
  //     return element.id == req.params.id
  //   }usuarios.json.bak
  // )
  //
  // console.log("indexOfElement es " + indexOfElement);
  // if (indexOfElement >= 0) {
  //   users.splice(indexOfElement, 1);
  //   deleted = true;
  // }

/* FRAN
    //------Formas de recorrer arrays-------

      //----For normal-----
      for (var i = 0; i < users.length; i++){
        console.log(i);
      }
      //-----forEach------

      users.forEach( function(valor, indice, array){
        console.log("En el índice " + indice + " hay este valor: " valor);
      });

      //--------for (element in object) --------
      //-------- Itera en las propiedades enumerables de un objeto.
      for (var property1 in users) {
        string1 += users[property1];
      }
      console.log(string1);

      //--------for (element of iterable) --------
      for (let value of users) {
        value += 1;
        console.log(value);
      }
      //----------Array findIndex --------
      function findFirstLargeNumber(element) {
      return element > 13;
      }
      console.log(users.findIndex(findFirstLargeNumber));
FRAN */
