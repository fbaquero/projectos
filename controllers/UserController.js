const io = require('../io.js');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjb9ed/collections/";
//const mLabAPIKey = "apiKey=AyhxbuxomkoEGTqUPWJaopASSBMShyJ4";
const crypt = require('../crypt')
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
/*
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujae9ed/collections/";
const mLabAPIKey = "apiKey=UFpKx5sIiLJUs6pQZ_1e676XvApZKxzI";
*/
function getUsersV1(req,res) {
  console.log('GET /apitechu/v1/users');

  //res.sendFile('usuarios.json', {root :__dirname});

  var result = {};  //donde almacenar despues  los resultados.
  var users = require('../usuarios.json');

  if (req.query.$count == "true"){
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
  users.slice(0, req.query.$top) : users;

  res.send(result);
}//EndFunction

function getUsersV2(req, res) {
 console.log('GET /apitechu/v2/users');

 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 httpClient.get("user?" + mLabAPIKey,
   function(err, resMlab, body) {
     var response = !err ? body : {
       "msg" : "Error obteniendo usuarios"
     }

     res.send(response);
   }
 )
}


function getUsersByIdV2(req, res) {
 console.log('GET /apitechu/v2/users/:id');

 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 var id = req.params.id;
 console.log("Id del usuario a traer " + id);
 var query = 'q={"id":' + id + '}';

 httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMlab, body) {
     if (err){
       var response= {
                    "msg" :"Error obteniendo usuario"
                     }
       res.status(500);
       }else{

         if (body.length > 0 ){
           var response = body[0]
         }else {
           var response = {
             "msg" : "Usuario no encontrado"
           }
           res.status(404);
         }
       }

     res.send(response);
   }
 )
}


function createUsersV1(req,res){
  console.log('POST /apitechu/v1/users');
  //Desde el frontal enviamos
  // los datos para que el usuario meta los datos
  //En backend metemoscreateUsersV1 el registro de los headers.
  //console.log(req.headers);

  console.log("adding firstbody_name: " + req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser= {
    first_name:req.body.first_name,
    last_name:req.body.last_name,
    email:req.body.email
  }

  var users = require('../usuarios.json');
  users.push(newUser);
  console.log ("Usuario añadido");
  //Para persistir esos datos tenemos que guardarlo. Por l oque trabajamos con la librería que acabamos de cargar FS FileSystem
  io.writeUserDataToFile(users);
}

function createUsersV2(req, res) {
   console.log("POST /apitechu/v2/users");
   console.log(req.body.id);
   console.log(req.body.first_name);
   console.log(req.body.last_name);
   console.log(req.body.email);
   console.log(req.body.password);

   var newUser = {
     "id": req.body.id,
     "first_name": req.body.first_name,
     "last_name": req.body.last_name,
     "email": req.body.email,
     //"password": req.body.password
     "password" : crypt.hash(req.body.password)
   };

   var httpClient = requestJson.createClient(baseMlabURL);
   console.log("Client created");
   httpClient.post("user?" + mLabAPIKey, newUser,
      function(err,resMlab, body){
          console.log("usuario guardado con exito");
          res.status(201).send({"msg" : "Usuario guardado con exito"});

      })


}

function deleteUsersV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  var deleted = false;

  if (deleted) {
    // io.writeUserDataToFile(users);
    io.writeUserDataToFile(users);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}
/*
function AuthController (req,res){
  //Inicio de AuthController
  console.log("Iniciamos Controlador de Autenticación");

  var users = require('../usuariosPassword.json');
  //Almacenamos Email y Password recibidos.
  var email = req.body.email;
  var password = req.body.password;

  console.log("El Email recibido es: " + req.body.email);
  console.log("La contraseña recibida es: "+ req.body.password);

  //Buscamos Email en BD
  for (var i = 0; i < users.length; i++) {
  console.log("comparando " + users[i].email + " y " +  req.params.email)
    if (users[i].email == req.params.email) {
       console.log("El correo: " + users[i].email + "existe en BD");

       //----Password Check-----
       if (users[i].password == req.params.password) {
         console.log("El password coincide");
         //Añadimos Check "Logged"
         users[i].logged = true;
         io.writeUserDataToFile(users);
         res.send("Id: " + users[i].email + " Logado Correctamente con Check de marcado.");
         break;
       }else{
         console.log("El password NO coincide");
         res.send("Password incorrecto");
       }
       //----END Password Check------
     }else{
       console.log("El usuario no existe");
       res.send("Usuario no dado de alta");

     }
   }

 }//EndFunction AuthController

function logout (req,res){

}
*/
module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUsersV1 = createUsersV1;
module.exports.createUsersV2 = createUsersV2;
module.exports.deleteUsersV1 = deleteUsersV1;
