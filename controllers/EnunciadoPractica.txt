const io = require('../io');

function loginV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}

function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;





Requisitos

Puede ser en parejas

En Mockaroo añadir: password a los usuarios. -> DONE
Crear nuevo controlador AuthController -> DONE
Añadir operaciones a la API:-> DONE

Login

POST a /apitechu/v1/login

En el body un JSON tal que:

{ "email" : "test@test.com", "password" : "1234" } -> DONE

Buscar el usuario con email igual al enviado:
Si no está el email -> NOK. -> DONE
Si está el email: -> DONE
  Si el password es igual al enviado -> OK
  Si el password no es igual al enviado -> NOK

  Si es login correcto:

Añadir al usuario una propiedad "logged" : true
Persistir el cambio.
Devolver la id de usuario.
Ejemplo de respuestas:

Login incorrecto { "mensaje" : "Login incorrecto" }

Login correcto { "mensaje" : "Login correcto", "idUsuario" : 1 }

Logout

POST a /apitechu/v1/logout/:id

Id es la id del usuario a deslogar.

Tanto para logout correcto como incorrecto, devolver un mensaje informativo.

En caso de logout correcto, guardar el cambio en el archivo.

En el logout tenemos que quitar el campo logged del usuario (delete user.logged)

Ejemplo de respuestas:

Logout incorrecto { "mensaje" : "logout incorrecto" }

Logout correcto { "mensaje" : "logout correcto" "idUsuario" : 1 }
