const io = require('../io');

function loginV1(req,res){
  console.log("POST /apitechu/v1/login");
  console.log("Iniciamos Controlador de Autenticación");
  var users = require('../usuarios.json');
  var message = "";
  for (user in users){
    if  (user.email == req.body.email){
          console.log("Email encontrado en BD");
          console.log("Chequeando Password");
            if(user.password == req.body.password){
              console.log("Login Correcto");
              var loggedUserId = user.id;
              user.logged = true;
              console.log("User logged with id " + user.id);
              message = "Login Correcto";
              io.writeUserDataToFile(users);
              break;
            }
    else{
          console.log("El usuario o la password son incorrectos");
          message = "Login incorrecto, chequea Email/Password";
        }
    }//End if
  }//End For

//Preparamos mensaje a devolver.
var response = {
                "mensaje" : message,
                "idUsuario": loggedUserId
    };
res.send(response);
}//End function loginV1


function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout");

 var users = require('../usuarios.json');
 var message = "";
 for (user of users) {
   if (user.id == req.params.id) {
     console.log("Usuario encontrado, chequeando si usuario ya esta logado");
        if(user.logged === true){
           console.log("Usuario ya esta logado actualmente");
           console.log("Desmarcando etiqueta de usuario logado");
           //Borramos la etiqueta de logado.
           delete user.logged
           console.log("Usuario deslogado con UserId= " + user.id);
           var loggedoutUserId = user.id;
           message = "Logout Correcto";
           io.writeUserDataToFile(users);
           break;
        }//End if
   }//End if
   else{
    console.log("El usuario no se encuentra logado");
    message = "El usuario no tiene sesión iniciada";
   }
 }//End for

  var response = {
     "mensaje": message,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}

function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var email = req.body.email;
  var password = req.body.password;

  var query = 'q={"email":"' + email + '"}';
  console.log(query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var user = body [0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      //res.send(user);
      //if (user.password == password) {
      if (crypt.checkPassword(password, user.password)) {
        console.log("password ok");
        var loggedUserId = user.id;
        user.logged = true;
        console.log("Logged in user with id " + user.id);

        var putBody = '{"$set":{"logged":true}}';

        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(err, resMLab, body) {
            console.log("Usuario guardado con exito");
          }
        )
      }
      var msg = loggedUserId ?
        "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

      var response = {
          "mensaje": msg,
          "idUsuario": loggedUserId
      };
      console.log(response);
      res.send(response);
    }
  )
}//End Function loginV2

function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 var id = req.params.id;
 console.log(id);

 var query = 'q={"id":' + id + '}';

 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMlab, body) {

     if (err) {
       var response = {
         "msg" : "Error obteniendo usuario"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var user = body [0];
       } else {
         var user = {
           "msg" : "Usuario no encontrado"
         }
         res.status(404);
       }
     }
     //res.send(user);

     if (user.logged === true) {
       console.log("User found, logging out");

       var putBody = '{"$unset":{"logged":""}}';

       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("Usuario guardado con exito");
         }
       )
       console.log("Logged out user with id " + user.id);
       var loggedoutUserId = user.id;
     }
     var msg = loggedoutUserId ?
       "Logout correcto" : "Logout incorrecto";

     var response = {
         "mensaje": msg,
         "idUsuario": loggedoutUserId
     };

     res.send(response);
   }
 )

}





module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;

/*-------Versión oficial del curso V1-------
function loginV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}

function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}
*/
/*  ------Versión oficial V2-----
function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");

 var query = 'q={"id":' + id + '}';
 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}


*/
